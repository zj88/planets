package com.planets.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.planets.gameobjects.Button;
import com.planets.gameobjects.Planet;
import com.planets.helpers.AssetLoader;
import com.planets.helpers.Metrics;

/**
 * Created by Z
 */
public class StartScreen implements Screen {

    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Game game;
    private Button startBtn;
    private DelayedRemovalArray<Planet> planets;

    public StartScreen(Game game) {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Metrics.GAME_WIDTH, Metrics.GAME_HEIGHT);
        batch.setProjectionMatrix(camera.combined);
        startBtn = new Button(Metrics.MID_POINT.x - AssetLoader.startBtn.getRegionWidth() / 2,
                Metrics.MID_POINT.y - AssetLoader.startBtn.getRegionHeight() - 30, AssetLoader.startBtn, null);
        this.game = game;
        planets = new DelayedRemovalArray<Planet>();
        spawnRandomPlanets();
    }

    private void spawnRandomPlanets() {
        int gameLevel = MathUtils.random(0, 3);
        for (int i = 0; i < 10; i++) {
            float x = MathUtils.random(0, Metrics.GAME_WIDTH);
            float y = MathUtils.random(0, Metrics.GAME_HEIGHT);
            int size = MathUtils.random(0, 3);
            planets.add(new Planet(x, y, new Vector2(75 + 15 * gameLevel, 0), AssetLoader.planets[gameLevel][size], 0));
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        for (Planet planet : planets) {
            planet.update(delta);
            planet.draw(batch);
        }
        batch.draw(AssetLoader.logo, Metrics.MID_POINT.x - AssetLoader.logo.getRegionWidth() / 2, Metrics.MID_POINT.y);
        startBtn.draw(batch);
        batch.end();
        if (Gdx.input.justTouched()) {
            Vector3 cords = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            if (startBtn.getBounds().contains(cords.x, cords.y)) {
                game.setScreen(new GameScreen());
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
