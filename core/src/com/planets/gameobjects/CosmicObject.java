package com.planets.gameobjects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.planets.helpers.Metrics;

/**
 * Created by Z
 */
public abstract class CosmicObject {

    protected Vector2 position;
    protected Vector2 velocity;
    protected float width;
    protected float height;
    protected TextureRegion textureRegion;
    protected Circle bounds;

    protected CosmicObject(float x, float y, TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
        width = textureRegion.getRegionWidth();
        height = textureRegion.getRegionHeight();
        position = new Vector2(x, y);
        bounds = new Circle(position.x + width / 2, position.y + height / 2, width / 2);
    }

    public void update(float delta) {
        if (position.x <= -width) {
            position.x = Metrics.GAME_WIDTH - width;
        }
        if (position.x >= Metrics.GAME_WIDTH) {
            position.x = 0;
        }
        if (position.y <= -height) {
            position.y = Metrics.GAME_HEIGHT - height;
        }
        if (position.y >= Metrics.GAME_HEIGHT) {
            position.y = 0;
        }
        bounds = new Circle(position.x + width / 2, position.y + height / 2, width / 2);
    }

    public void stopObject() {
        velocity = new Vector2(0, 0);
    }

    public int getX() {
        return (int) position.x;
    }

    public int getY() {
        return (int) position.y;
    }

    public Circle getBounds() {
        return bounds;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }
}
