package com.planets.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.planets.helpers.Metrics;

/**
 * Created by Z
 */
public class Planet extends CosmicObject {

    private int mass;
    private int dmg;
    private Circle[] boundsArray;
    private int level;

    public Planet(float x, float y, Vector2 velocity, TextureRegion textureRegion, int level) {
        super(x, y, textureRegion);
        this.level = level;
        dmg = 10 * (4 - level);
        mass = 1 * (4 - level);
        int velRotation = MathUtils.random(0, 360);
        this.velocity = new Vector2(velocity.x + level * 15, 0);
        this.velocity.rotate(velRotation);
        boundsArray = new Circle[9];
        updateBounds();
    }

    private void updateBounds() {
        float radius = width / 2 * 0.65f;
        boundsArray[0] = new Circle(position.x + width / 2, position.y + height / 2, radius);
        boundsArray[1] = new Circle(position.x + width / 2, position.y + height / 2 - Metrics.GAME_HEIGHT, radius);
        boundsArray[2] = new Circle(position.x + width / 2, position.y + height / 2 + Metrics.GAME_HEIGHT, radius);
        boundsArray[3] = new Circle(position.x + width / 2 - Metrics.GAME_WIDTH, position.y + height / 2, radius);
        boundsArray[4] = new Circle(position.x + width / 2 - Metrics.GAME_WIDTH, position.y + height / 2 - Metrics.GAME_HEIGHT, radius);
        boundsArray[5] = new Circle(position.x + width / 2 - Metrics.GAME_WIDTH, position.y + height / 2 + Metrics.GAME_HEIGHT, radius);
        boundsArray[6] = new Circle(position.x + width / 2 + Metrics.GAME_WIDTH, position.y + height / 2, radius);
        boundsArray[7] = new Circle(position.x + width / 2 + Metrics.GAME_WIDTH, position.y + height / 2 - Metrics.GAME_HEIGHT, radius);
        boundsArray[8] = new Circle(position.x + width / 2 + Metrics.GAME_WIDTH, position.y + height / 2 + Metrics.GAME_HEIGHT, radius);
    }

    public boolean isColliding(Circle circle) {
        for (Circle bound : boundsArray) {
            if (bound.overlaps(circle)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void update(float delta) {
        position.add(velocity.cpy().scl(delta));
        super.update(delta);
        updateBounds();
    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, position.x, position.y, width, height);
        spriteBatch.draw(textureRegion, position.x, position.y - Metrics.GAME_HEIGHT, width, height);
        spriteBatch.draw(textureRegion, position.x, position.y + Metrics.GAME_HEIGHT, width, height);
        spriteBatch.draw(textureRegion, position.x - Metrics.GAME_WIDTH, position.y, width, height);
        spriteBatch.draw(textureRegion, position.x - Metrics.GAME_WIDTH, position.y - Metrics.GAME_HEIGHT, width, height);
        spriteBatch.draw(textureRegion, position.x - Metrics.GAME_WIDTH, position.y + Metrics.GAME_HEIGHT, width, height);
        spriteBatch.draw(textureRegion, position.x + Metrics.GAME_WIDTH, position.y, width, height);
        spriteBatch.draw(textureRegion, position.x + Metrics.GAME_WIDTH, position.y - Metrics.GAME_HEIGHT, width, height);
        spriteBatch.draw(textureRegion, position.x + Metrics.GAME_WIDTH, position.y + Metrics.GAME_HEIGHT, width, height);
    }

    public Circle[] getBoundsArray() {
        return boundsArray;
    }

    public int getLevel() {
        return level;
    }

    public int getDmg() {
        return dmg;
    }

    public int getMass() {
        return mass;
    }
}
