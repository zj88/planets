package com.planets.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.planets.helpers.AssetLoader;

/**
 * Created by Z
 */
public class Bullet extends CosmicObject {

    private static final float LIFE_TIME = 1.1f;
    private boolean isAlive;
    private float lifeTime;

    public Bullet(float x, float y, Vector2 velocity) {
        super(x, y, AssetLoader.bullet);
        this.velocity = velocity;
        isAlive = true;
        lifeTime = LIFE_TIME;
    }

    @Override
    public void update(float delta) {
        lifeTime -= delta;
        if (lifeTime <= 0) {
            isAlive = false;
        }
        position.add(velocity.cpy().scl(delta));
        super.update(delta);
    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, position.x, position.y, width, height);
    }

    public boolean isAlive() {
        return isAlive;
    }
}
