package com.planets.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Z on 2014-08-28.
 */

public class Button extends CosmicObject {

    private Action action;
    private boolean isVisible;

    public Button(float x, float y, TextureRegion textureRegion, Action action) {
        super(x, y, textureRegion);
        if (action == Action.FIRE || action == Action.SHIELD) {
            width = textureRegion.getRegionWidth() * 1.5f;
            height = textureRegion.getRegionHeight() * 1.5f;
            position = new Vector2(x, y);
            bounds = new Circle(position.x + width / 2, position.y + height / 2, width / 2);
        }
        this.action = action;
        isVisible = true;
    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, position.x, position.y, width, height);
    }

    public Action getAction() {
        return action;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public static enum Action {
        FIRE,
        SHIELD,
        RESTART
    }
}
