package com.planets.gameobjects;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.planets.gameworld.GameWorld;
import com.planets.helpers.AssetLoader;
import com.planets.helpers.Joystick;
import com.planets.helpers.Metrics;

import static com.badlogic.gdx.math.MathUtils.cosDeg;
import static com.badlogic.gdx.math.MathUtils.sinDeg;

/**
 * Created by Z on 2014-08-24.
 */

public class Starship extends CosmicObject {

    private static final float ROTATION_SPEED = 160f;
    private static final float V = 160f;
    private static final int MAX_SPEED = 500;
    private static final float SHIELD_DECAY_TIME = 0.5f;
    private Joystick joystick;
    private boolean movingLeft;
    private boolean movingRight;
    private boolean movingForward;
    private float rotation;
    private Animation animation;
    private float runTime;
    private GameWorld world;
    private boolean isAlive;
    private boolean shieldUp;
    private int shieldEnergy;
    private int mass;
    private float shieldUpTime;

    public Starship(float x, float y, GameWorld world) {
        super(x, y, AssetLoader.starship);
        animation = AssetLoader.animation;
        velocity = new Vector2(0, 0);
        this.world = world;
        isAlive = true;
        rotation = 90;
        shieldEnergy = 100;
        mass = 200;
        joystick = world.getJoystick();
    }

    @Override
    public void update(float delta) {
        runTime += delta;

        if (isAlive) {
            if (movingLeft) {
                rotation += ROTATION_SPEED * delta;
            }
            if (movingRight) {
                rotation -= ROTATION_SPEED * delta;
            }
            if (movingForward) {
                velocity.x += V * cosDeg(rotation) * delta;
                velocity.y += V * sinDeg(rotation) * delta;
                textureRegion = animation.getKeyFrame(runTime);
            } else {
                textureRegion = AssetLoader.starship;
            }

            if (Gdx.app.getType() == Application.ApplicationType.Android) {
                if (Math.abs(joystick.getKnobPercentX()) > 0.2) {
                    rotation -= ROTATION_SPEED * delta * joystick.getKnobPercentX();
                }
                if (joystick.getKnobPercentY() > 0) {
                    velocity.x += V * cosDeg(rotation) * delta * joystick.getKnobPercentY();
                    velocity.y += V * sinDeg(rotation) * delta * joystick.getKnobPercentY();
                    textureRegion = animation.getKeyFrame(runTime);
                } else {
                    textureRegion = AssetLoader.starship;
                }
            }

            if (velocity.len() > MAX_SPEED) {
                velocity.scl(MAX_SPEED / velocity.len());
            }
            position.add(velocity.cpy().scl(delta));
            super.update(delta);

            if (shieldEnergy <= 0) {
                shieldUp = false;
                shieldEnergy = 0;
            }
            if (shieldUp) {
                bounds.setRadius(AssetLoader.shield.getRegionWidth() / 2);
                if (shieldUpTime >= SHIELD_DECAY_TIME) {
                    shieldEnergy -= 1;
                    shieldUpTime = 0;
                } else {
                    shieldUpTime += delta;
                }
            } else {
                bounds.radius *= 0.8f;
            }
        }
    }

    @Override
    public void stopObject() {
        super.stopObject();
        isAlive = false;
    }

    public void fire() {
        if (isAlive) {
            Vector2 bulletVelocity = new Vector2(450, 0);
            bulletVelocity.rotate(rotation);
            Vector2 bulletPosition = new Vector2(23, -1);
            bulletPosition.rotate(rotation);
            Bullet bullet = new Bullet(position.x + width / 2 + bulletPosition.x - AssetLoader.bullet.getRegionWidth() / 2,
                    position.y + height / 2 + bulletPosition.y, bulletVelocity);
            world.getBullets().add(bullet);
        }
    }

    public void draw(SpriteBatch spriteBatch) {
        if (isAlive) {
            spriteBatch.draw(textureRegion, position.x, position.y, width / 2, height / 2, width, height, 1, 1, rotation);
            if (shieldUp) {
                spriteBatch.draw(AssetLoader.shield, position.x + width / 2 - AssetLoader.shield.getRegionWidth() / 2,
                        position.y + height / 2 - AssetLoader.shield.getRegionHeight() / 2);
            }
        }
    }

    public void reset() {
        position.x = Metrics.GAME_WIDTH / 2.0f - width / 2.0f;
        position.y = Metrics.GAME_HEIGHT / 2.0f - height / 2.0f;
        velocity.x = 0;
        velocity.y = 0;

        shieldEnergy = 100;
        rotation = 90;
        isAlive = true;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public int getShieldEnergy() {
        return shieldEnergy;
    }

    public void setShieldEnergy(int shieldEnergy) {
        this.shieldEnergy = shieldEnergy;
    }

    public boolean isShieldUp() {
        return shieldUp;
    }

    public void setShieldUp(boolean shieldUp) {
        this.shieldUp = shieldUp;
    }

    public int getMass() {
        return mass;
    }

    public void setMovingLeft(boolean movingLeft) {
        this.movingLeft = movingLeft;
    }

    public void setMovingRight(boolean movingRight) {
        this.movingRight = movingRight;
    }

    public void setMovingForward(boolean movingForward) {
        this.movingForward = movingForward;
    }
}
