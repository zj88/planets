package com.planets.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Z
 */
public class Metrics {

    public static final float GAME_HEIGHT = 720.0f;
    public static float SCREEN_WIDTH = Gdx.graphics.getWidth();
    public static float SCREEN_HEIGHT = Gdx.graphics.getHeight();
    public static final float GAME_WIDTH = (SCREEN_WIDTH / SCREEN_HEIGHT) * GAME_HEIGHT;
    public static final Vector2 MID_POINT = new Vector2(GAME_WIDTH / 2.0f, GAME_HEIGHT / 2.0f);
    public static final Vector2 SCALE = new Vector2(GAME_WIDTH / SCREEN_WIDTH, GAME_HEIGHT / SCREEN_HEIGHT);
    public static final int BETWEEN_LVL_DELAY = 2;
    public static final boolean DEBUG = false;
}
