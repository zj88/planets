package com.planets.helpers;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by Z
 */
public class Joystick {

    private Touchpad touchpad;
    private Drawable joystickKnob;
    private Drawable joystickBackground;
    private Touchpad.TouchpadStyle joystickStyle;
    private Skin joystickSkin;

    public Joystick() {
        joystickSkin = new Skin();
        joystickSkin.addRegions(AssetLoader.atlas);
        joystickStyle = new Touchpad.TouchpadStyle();
        joystickBackground = new TextureRegionDrawable(joystickSkin.get("touchBackground", TextureRegion.class));
        joystickKnob = new TextureRegionDrawable(joystickSkin.get("touchKnob", TextureRegion.class));
        joystickStyle.background = joystickBackground;
        joystickStyle.knob = joystickKnob;
        touchpad = new Touchpad(10, joystickStyle);
        touchpad.setBounds(Metrics.SCREEN_WIDTH - 20 - AssetLoader.touchBackground.getRegionWidth() * 2, 20, AssetLoader.touchBackground.getRegionWidth() * 2, AssetLoader.touchBackground.getRegionHeight() * 2);
    }

    public Touchpad getTouchpad() {
        return touchpad;
    }

    public float getKnobPercentX() {
        return touchpad.getKnobPercentX();
    }

    public float getKnobPercentY() {
        return touchpad.getKnobPercentY();
    }
}
