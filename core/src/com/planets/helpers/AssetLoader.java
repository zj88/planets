package com.planets.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Z on 2014-08-24.
 */
public class AssetLoader {

    public static TextureRegion starship;
    public static TextureRegion starshipFired1, starshipFired2;
    public static TextureRegion bullet;
    public static Animation animation;
    public static TextureRegion[][] planets;
    public static ParticleEffect explosion;
    public static TextureRegion arrowUp;
    public static TextureRegion arrowLeft;
    public static TextureRegion arrowRight;
    public static TextureRegion fireBtn;
    public static TextureRegion shieldBtn;
    public static TextureRegion restartBtn;
    public static TextureRegion shield;
    public static BitmapFont font;
    public static TextureAtlas atlas;
    public static TextureRegion logo;
    public static TextureRegion startBtn;
    public static TextureRegion touchBackground;
    public static TextureRegion touchKnob;

    public static void load() {
        atlas = new TextureAtlas("textures.pack");

        starship = atlas.findRegion("starship2");
        starshipFired1 = atlas.findRegion("starship2-2");
        starshipFired2 = atlas.findRegion("starship2-3");
        bullet = atlas.findRegion("bullet");

        planets = new TextureRegion[4][];
        planets[0] = new TextureRegion[4];
        planets[1] = new TextureRegion[4];
        planets[2] = new TextureRegion[4];
        planets[3] = new TextureRegion[4];

        planets[0][0] = atlas.findRegion("planet11");
        planets[0][1] = atlas.findRegion("planet12");
        planets[0][2] = atlas.findRegion("planet13");
        planets[0][3] = atlas.findRegion("planet14");

        planets[1][0] = atlas.findRegion("planet21");
        planets[1][1] = atlas.findRegion("planet22");
        planets[1][2] = atlas.findRegion("planet23");
        planets[1][3] = atlas.findRegion("planet24");

        planets[2][0] = atlas.findRegion("planet31");
        planets[2][1] = atlas.findRegion("planet32");
        planets[2][2] = atlas.findRegion("planet33");
        planets[2][3] = atlas.findRegion("planet34");

        planets[3][0] = atlas.findRegion("planet41");
        planets[3][1] = atlas.findRegion("planet42");
        planets[3][2] = atlas.findRegion("planet43");
        planets[3][3] = atlas.findRegion("planet44");

        animation = new Animation(0.06f, starshipFired1, starshipFired2);
        animation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        explosion = new ParticleEffect();
        explosion.load(Gdx.files.internal("blueExp"), atlas);

        arrowUp = atlas.findRegion("arrow_up");
        arrowLeft = atlas.findRegion("arrow_left");
        arrowRight = atlas.findRegion("arrow_right");
        fireBtn = atlas.findRegion("fire_btn");
        shieldBtn = atlas.findRegion("shield_btn");
        restartBtn = atlas.findRegion("restart_btn");

        shield = atlas.findRegion("shield");
        font = new BitmapFont(Gdx.files.internal("font.fnt"), atlas.findRegion("font"));

        logo = atlas.findRegion("logo");
        startBtn = atlas.findRegion("start_btn");

        touchBackground = atlas.findRegion("touchBackground");
        touchKnob = atlas.findRegion("touchKnob");
    }

    public static void dispose() {
        atlas.dispose();
    }
}
