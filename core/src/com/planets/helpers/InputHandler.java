package com.planets.helpers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Array;
import com.planets.gameobjects.Button;
import com.planets.gameobjects.Starship;
import com.planets.gameworld.GameWorld;

/**
 * Created by Z on 2014-08-24.
 */
public class InputHandler implements InputProcessor {

    private final Array<Button> buttons;
    private Starship starship;
    private GameWorld world;

    public InputHandler(GameWorld world) {
        this.world = world;
        starship = world.getStarship();
        buttons = world.getButtons();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.LEFT) {
            starship.setMovingLeft(true);
        } else if (keycode == Input.Keys.RIGHT) {
            starship.setMovingRight(true);
        }
        if (keycode == Input.Keys.UP) {
            starship.setMovingForward(true);
        }
        if (keycode == Input.Keys.A) {
            starship.fire();
        }
        if ((starship.getShieldEnergy() > 0) && keycode == Input.Keys.S) {
            starship.setShieldUp(true);
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.LEFT) {
            starship.setMovingLeft(false);
        } else if (keycode == Input.Keys.RIGHT) {
            starship.setMovingRight(false);
        }
        if (keycode == Input.Keys.UP) {
            starship.setMovingForward(false);
        }
        if ((starship.getShieldEnergy() <= 0) || keycode == Input.Keys.S) {
            starship.setShieldUp(false);
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenX *= Metrics.SCALE.x;
        screenY *= Metrics.SCALE.y;
        screenY = (int) (Metrics.GAME_HEIGHT - screenY);
        for (Button cometButton : buttons) {
            if (cometButton.getBounds().contains(screenX, screenY)) {
                if (cometButton.getAction() == Button.Action.FIRE) {
                    starship.fire();
                }
                if ((starship.getShieldEnergy() > 0) && cometButton.getAction() == Button.Action.SHIELD) {
                    starship.setShieldUp(true);
                }
                if (cometButton.getAction() == Button.Action.RESTART && cometButton.isVisible()) {
                    cometButton.setVisible(false);
                    world.restart();
                }
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenX *= Metrics.SCALE.x;
        screenY *= Metrics.SCALE.y;
        screenY = (int) (Metrics.GAME_HEIGHT - screenY);
        for (Button cometButton : buttons) {
            if (cometButton.getBounds().contains(screenX, screenY)) {
                if ((starship.getShieldEnergy() <= 0) || cometButton.getAction() == Button.Action.SHIELD) {
                    starship.setShieldUp(false);
                }
            }
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
