package com.planets;

import com.badlogic.gdx.Game;
import com.planets.helpers.AssetLoader;
import com.planets.screens.StartScreen;

/**
 * Created by Z
 */
public class PlanetsGame extends Game {

    @Override
    public void create() {
        AssetLoader.load();
        setScreen(new StartScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }
}
