package com.planets.gameworld;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.planets.gameobjects.Bullet;
import com.planets.gameobjects.Button;
import com.planets.gameobjects.CosmicObject;
import com.planets.gameobjects.Planet;
import com.planets.gameobjects.Starship;
import com.planets.helpers.AssetLoader;
import com.planets.helpers.Joystick;
import com.planets.helpers.Metrics;

/**
 * Created by Z on 2014-08-24.
 */

public class GameWorld {

    private Joystick joystick;
    private Starship starship;
    private DelayedRemovalArray<Bullet> bullets;
    private DelayedRemovalArray<Planet> planets;
    private ParticleEffectPool explosionEffectPool;
    private Array<ParticleEffectPool.PooledEffect> effects;
    private Array<Button> buttons;
    private Button restartButton;
    private int gameLevel;
    private float betweenLevelDelay;

    public GameWorld() {
        joystick = new Joystick();
        this.starship = new Starship(Metrics.MID_POINT.x, Metrics.MID_POINT.y, this);
        bullets = new DelayedRemovalArray<Bullet>();
        planets = new DelayedRemovalArray<Planet>();
        spawnPlanets();
        explosionEffectPool = new ParticleEffectPool(AssetLoader.explosion, 5, 30);
        effects = new Array<ParticleEffectPool.PooledEffect>();
        buttons = new Array<Button>();
        createButtons();
        betweenLevelDelay = Metrics.BETWEEN_LVL_DELAY;
    }

    private void spawnPlanets() {
        int numberOfPlanets = 2 + gameLevel;
        for (int i = 0; i < numberOfPlanets; i++) {
            float x = Metrics.MID_POINT.x;
            float y = Metrics.MID_POINT.y;
            while (x > (Metrics.MID_POINT.x - AssetLoader.planets[gameLevel % 4][0].getRegionWidth() * 1.5) &&
                    x < (Metrics.MID_POINT.x + AssetLoader.planets[gameLevel % 4][0].getRegionWidth() * 0.5)) {
                x = MathUtils.random(0, Metrics.GAME_WIDTH - AssetLoader.planets[gameLevel % 4][0].getRegionWidth());
            }
            while (y > Metrics.MID_POINT.y - AssetLoader.planets[gameLevel % 4][0].getRegionHeight() * 1.5 &&
                    y < Metrics.MID_POINT.y + AssetLoader.planets[gameLevel % 4][0].getRegionHeight() * 0.5) {
                y = MathUtils.random(0, Metrics.GAME_HEIGHT - AssetLoader.planets[gameLevel % 4][0].getRegionHeight());
            }
            planets.add(new Planet(x, y, new Vector2(70 + 10 * gameLevel, 0), AssetLoader.planets[gameLevel % 4][0], 0));
        }
    }

    private void createButtons() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            buttons.add(new Button(20, 20, AssetLoader.fireBtn, Button.Action.FIRE));
            buttons.add(new Button(40 + AssetLoader.fireBtn.getRegionWidth() * 1.5f, 20, AssetLoader.shieldBtn, Button.Action.SHIELD));
        }
        restartButton = new Button(Metrics.GAME_WIDTH / 2f - AssetLoader.restartBtn.getRegionWidth() / 2f, Metrics.GAME_HEIGHT / 2f - AssetLoader.restartBtn.getRegionHeight() / 2,
                AssetLoader.restartBtn, Button.Action.RESTART);
        restartButton.setVisible(false);
        buttons.add(restartButton);

    }

    public void update(float delta) {
        starship.update(delta);
        for (Bullet bullet : bullets) {
            if (!bullet.isAlive()) {
                bullets.removeValue(bullet, true);
            }
            bullet.update(delta);
        }
        for (Planet planet : planets) {
            planet.update(delta);
        }

        checkCollisions();

        for (int i = effects.size - 1; i >= 0; i--) {
            ParticleEffectPool.PooledEffect effect = effects.get(i);
            effect.update(delta);
            if (effect.isComplete()) {
                effect.free();
                effects.removeIndex(i);
            }
        }

        checkForNextLevel(delta);
    }

    private void checkCollisions() {
        for (Planet planet : planets) {
            for (Bullet bullet : bullets) {
                if (planet.isColliding(bullet.getBounds())) {
                    collide(planet);
                    bullets.removeValue(bullet, true);
                }
            }
            if (starship.isAlive() && planet.isColliding(starship.getBounds())) {
                collide(planet);
                if (starship.isShieldUp()) {
                    collideShip(planet);
                    starship.setShieldEnergy(starship.getShieldEnergy() - planet.getDmg());
                } else {
                    gameOver();
                    break;
                }
            }
        }
    }

    private void checkForNextLevel(float delta) {
        if (planets.size <= 0) {
            if (betweenLevelDelay <= 0) {
                gameLevel++;
                spawnPlanets();
                betweenLevelDelay = Metrics.BETWEEN_LVL_DELAY;
            } else {
                betweenLevelDelay -= delta;
            }
        }
    }

    private void collide(Planet planet) {
        if (planet.getLevel() < 3) {
            int level = planet.getLevel() + 1;
            planets.add(new Planet(planet.getX() + planet.getWidth() / 4, planet.getY() + planet.getHeight() / 4, new Vector2(75 + 15 * gameLevel, 0),
                    AssetLoader.planets[gameLevel][level], level));
            planets.add(new Planet(planet.getX() + planet.getWidth() / 4, planet.getY() + planet.getHeight() / 4, new Vector2(75 + 15 * gameLevel, 0),
                    AssetLoader.planets[gameLevel][level], level));
        }
        ParticleEffectPool.PooledEffect effect = explosionEffectPool.obtain();
        effect.setPosition(planet.getX() + planet.getWidth() / 2, planet.getY() + planet.getHeight() / 2);
        effect.start();
        effects.add(effect);
        planets.removeValue(planet, true);
    }

    private void collideShip(Planet planet) {
//        newVelX =
//                (firstBall.speed.x * (firstBall.mass – secondBall.mass) + (2 * secondBall.mass * secondBall.speed.x))
//        / (firstBall.mass + secondBall.mass);
        float newVelX1 = (starship.getVelocity().x * (AssetLoader.shield.getRegionWidth() - planet.getWidth()) +
                (2 * planet.getWidth() * planet.getVelocity().x)) / (AssetLoader.shield.getRegionWidth() + planet.getWidth());
        float newVelY1 = (starship.getVelocity().y * (AssetLoader.shield.getRegionWidth() - planet.getWidth()) +
                (2 * planet.getWidth() * planet.getVelocity().y)) / (AssetLoader.shield.getRegionWidth() + planet.getWidth());
        float newVelX2 = (planet.getVelocity().x * (planet.getWidth() - AssetLoader.shield.getRegionWidth()) +
                (2 * AssetLoader.shield.getRegionWidth() * starship.getVelocity().x)) / (AssetLoader.shield.getRegionWidth() + planet.getWidth());
        float newVelY2 = (planet.getVelocity().y * (planet.getWidth() - AssetLoader.shield.getRegionWidth()) +
                (2 * AssetLoader.shield.getRegionWidth() * starship.getVelocity().y)) / (AssetLoader.shield.getRegionWidth() + planet.getWidth());
        starship.setVelocity(new Vector2(newVelX1, newVelY1));
        planet.setVelocity(new Vector2(newVelX2, newVelY2));
//        Vector2 shipCentre = getCentre(starship);
//        Vector2 planetCentre = getCentre(planet);
//        float distance = (float) Math.sqrt(Math.pow(shipCentre.x - planetCentre.x, 2) + Math.pow(shipCentre.y - planetCentre.y, 2));
//        Vector2 norm = new Vector2((planetCentre.x - shipCentre.x) / distance, (planetCentre.y - shipCentre.y) / distance);
//        float p = 2 * (starship.getVelocity().x * norm.x + starship.getVelocity().y * norm.y -
//                planet.getVelocity().x * norm.x - planet.getVelocity().y * norm.y) / (starship.getMass() + planet.getMass());
//        starship.setVelocity(new Vector2(starship.getVelocity().x - p * starship.getMass() * norm.x,
//                starship.getVelocity().y - p * starship.getMass() * norm.y));
//        planet.setVelocity(new Vector2(planet.getVelocity().x + p * planet.getMass() * norm.x,
//                planet.getVelocity().y + p * planet.getMass() * norm.y));
    }

    private void gameOver() {
        starship.stopObject();
        //stopAllObjects();
        restartButton.setVisible(true);
    }

    private Vector2 getCentre(CosmicObject cosmicObject) {
        return new Vector2(cosmicObject.getX() + cosmicObject.getWidth() / 2, cosmicObject.getY() + cosmicObject.getHeight() / 2);
    }

    private void stopAllObjects() {
        for (Planet planet : planets) {
            planet.stopObject();
        }
        for (Bullet bullet : bullets) {
            bullet.stopObject();
        }
        starship.stopObject();
    }

    public void restart() {
        starship.reset();
        planets.clear();
        bullets.clear();
        spawnPlanets();
    }

    public Starship getStarship() {
        return starship;
    }

    public DelayedRemovalArray<Bullet> getBullets() {
        return bullets;
    }

    public DelayedRemovalArray<Planet> getPlanets() {
        return planets;
    }

    public Array<ParticleEffectPool.PooledEffect> getEffects() {
        return effects;
    }

    public Array<Button> getButtons() {
        return buttons;
    }

    public float getBetweenLevelDelay() {
        return betweenLevelDelay;
    }

    public int getGameLevel() {
        return gameLevel;
    }

    public Joystick getJoystick() {
        return joystick;
    }
}
