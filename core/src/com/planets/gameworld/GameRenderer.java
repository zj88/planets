package com.planets.gameworld;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.planets.gameobjects.Bullet;
import com.planets.gameobjects.Button;
import com.planets.gameobjects.Planet;
import com.planets.gameobjects.Starship;
import com.planets.helpers.AssetLoader;
import com.planets.helpers.InputHandler;
import com.planets.helpers.Joystick;
import com.planets.helpers.Metrics;

/**
 * Created by Z on 2014-08-24.
 */
public class GameRenderer {

    private ShapeRenderer shapeRenderer;
    private Joystick joystick;
    private Stage stage;
    private Viewport viewport;
    private GameWorld world;
    private SpriteBatch batch;
    private Starship starship;
    private DelayedRemovalArray<Bullet> bullets;
    private OrthographicCamera camera;
    private DelayedRemovalArray<Planet> planets;
    private Array<ParticleEffectPool.PooledEffect> effects;

    public GameRenderer(GameWorld world) {
        this.world = world;
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Metrics.GAME_WIDTH, Metrics.GAME_HEIGHT);
        batch.setProjectionMatrix(camera.combined);
        starship = world.getStarship();
        bullets = world.getBullets();
        planets = world.getPlanets();
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);
        effects = world.getEffects();

        joystick = world.getJoystick();
        viewport = new ScreenViewport(camera);
        stage = new Stage(viewport);
        InputMultiplexer multiplexer = new InputMultiplexer();
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            stage.addActor(joystick.getTouchpad());
            multiplexer.addProcessor(stage);
        }
        multiplexer.addProcessor(new InputHandler(world));
        Gdx.input.setInputProcessor(multiplexer);
    }

    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        starship.draw(batch);
        for (Bullet bullet : bullets) {
            bullet.draw(batch);
        }
        for (Planet planet : planets) {
            planet.draw(batch);
        }
        for (ParticleEffectPool.PooledEffect effect : effects) {
            effect.draw(batch);
        }
        for (Button button : world.getButtons()) {
            if (button.isVisible()) {
                button.draw(batch);
            }
        }

        AssetLoader.font.draw(batch, "Shield: " + starship.getShieldEnergy() + "%", 30, Metrics.GAME_HEIGHT - 20);

        if (world.getBetweenLevelDelay() < Metrics.BETWEEN_LVL_DELAY) {
            String level = "Level " + (world.getGameLevel() + 2);
            AssetLoader.font.draw(batch, level, Metrics.MID_POINT.x - AssetLoader.font.getBounds(level).width / 2, Metrics.MID_POINT.y);
        }

        batch.end();

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        if (Metrics.DEBUG) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.WHITE);
            shapeRenderer.circle(starship.getBounds().x, starship.getBounds().y, starship.getBounds().radius);
            for (Planet planet : planets) {
                Circle[] planetBounds = planet.getBoundsArray();
                for (Circle planetBound : planetBounds) {
                    shapeRenderer.circle(planetBound.x, planetBound.y, planetBound.radius);
                }
            }
            shapeRenderer.end();
        }
    }
}
